<?php #! /usr/bin/php5 -q
################################################################################
#                       CGE SERVICE - TreeViewer                               #
################################################################################
# CONFIG VARIABLE
$service = "TreeViewer"; # EDIT! SERVICE
$version = "1.0"; # EDIT! VERSION

# STANDARD CBS PAGE TEMPLATES, always include this file
include_once('/srv/www/php-lib/cge_std-2.0.php'); // Including CGE_std clases and functions
$CGE = new CGE('CGE Server - TreeViewer', '',
               '/images/cge_buttons/banner.gif',
               'css/cbs.css,css/style.css,css/jquery.contextMenu.cs',
               'js/d3.v2.js,js/newick.js,js/newick2svg.js,js/tree-ui.js,js/jscolor/jscolor.js,js/svg2pdf.js'
              ); // Load the Class js/cbs.js,js/jquery.min.js,js/jquery-ui.min.js,

# CGE MENU
# Format is: ServerName, "(Link/Path.html, 'NameOfLink'),(Link/Path.html, 'NameOfLink')"
$CGE->std_header("$service $version - Phylogeny Tree Viewer", "(instructions.php,'Instructions'),(output.php,'Output'),(abstract.php,'Article abstract')"); // Print the Menu

// FORM HANDLER
function _INPUT($name)
{
    if ($_SERVER['REQUEST_METHOD'] == 'GET')
        return strip_tags($_GET[$name]);
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
        return strip_tags($_POST[$name]);
}

if (count($_POST)>0 or count($_GET)>0)
{
  $imageDir = trim(_INPUT("imagedir"));
  $newick = trim(_INPUT("newick"));
  $treeZip =  substr($imageDir, 0, strlen($imageDir)-4)."/TreeViewer.zip";
} else {
  $imageDir = "/services/TreeViewer-1.0/etc/";
  $newick = "tree.newick";
  $treeZip =  substr($imageDir, 0, strlen($imageDir)-4)."/TreeViewer.zip";
}

// FORM HANDLER ENDS
?>

<script type='text/javascript'>
    // UI SPECIFIC SCRIPT
    function ShowNewick(){
      // This fct uses the form values and converts the newick to a SVG Tree
		path = document.getElementById('newick').value;
      var dim = [parseInt(document.getElementById('dimx').value), parseInt(document.getElementById('dimy').value)];
      var margin = parseInt(document.getElementById('marg').value);
      var span = parseInt(document.getElementById('span').value);
      var rotate = parseInt(document.getElementById('rotation').value);
      var label = [document.getElementById('lsize').value, document.getElementById('lcolor').value, document.getElementById('lfont').value];
      var bootstrap = [document.getElementById('bsize').value, document.getElementById('bcolor').value, document.getElementById('bfont').value];
      Newick2Tree(path, dim, span, rotate, margin, label, bootstrap);
    }
    function ShowNewick2(){
      // This fct uses the form values and converts the newick to a SVG Tree
		var text = document.getElementById('newick_text').value;
      var dim = [parseInt(document.getElementById('dimx').value), parseInt(document.getElementById('dimy').value)];
      var margin = parseInt(document.getElementById('marg').value);
      var span = parseInt(document.getElementById('span').value);
      var rotate = parseInt(document.getElementById('rotation').value);
      var label = [document.getElementById('lsize').value, document.getElementById('lcolor').value, document.getElementById('lfont').value];
      var bootstrap = [document.getElementById('bsize').value, document.getElementById('bcolor').value, document.getElementById('bfont').value];
      Newick2Tree2(text, dim, span, rotate, margin, label, bootstrap);
    }
  //	 function keyEvent(e){
  //	   if(e.keyCode == 13){
  //      document.getElementById('submit').focus();
  //	   }
  //	 };
    window.imageDir = 'images/';
</script>
<style type="text/css">
   table tr td table{
      display: inline;
   }
   table tr td table tr td{
      width: 128px;
   }
</style>

<!-- START INDHOLD -->
<!--[if IE]>
<p class="bulk warn">The MHC TreeViewer is not supported by Internet Explorer, since IE does not follow the CSS standard, and have trouble executing javascripts. Please use another browser.</p>
<![endif]-->

<div style='text-align: center;width:1200px;'>
   <table>
      <tr>
<!-- SVG / IMAGE -->
         <td>
            <svg id='svg'
                 xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink"
                 version="1.1"></svg><br>
            <button onclick="svgdownload('png');">Download as PNG</button>
            <button onclick="svgdownload('pdf');">Download as PDF</button>
         </td>
<!-- SVG / IMAGE (END) -->
      </tr>
      <tr>
<!-- USER INTERACTION FORM -->
         <td>
            <form>
            <input type='hidden' id='newick' size='66' value='<?php echo $imageDir."/".$newick; ?>'>
            <hr>
            <h3>Change Tree parameters:</h3><br>
            <table class='splitview' style='display: inline;'>
               <tr>
                  <td align='left'>Dimension X: </td>
                  <td><input type='text' id='dimx' value='1024' size='4'></td>
               </tr><tr>
                  <td align='left'>Dimension Y: </td>
                  <td><input type='text' id='dimy' value='512' size='4'></td>
               </tr><tr>
                  <td align='left'>Margin: </td>
                  <td><input type='text' id='marg' value='0' size='4'></td>
               </tr><tr>
                  <td align='left'>Span: </td>
                  <td><input type='text' id='span' value='360' size='4'></td> 
               </tr><tr>
                  <td align='left'>Rotation: </td>
                  <td><input type='text' id='rotation' value='0' size='4'></td>
               </tr>
            </table><table style='display: inline;width:256px;'>
               <tr>
                  <td align='left'>Label size: </td>
                  <td><input id='lsize' size='2' value='8' onkeyup="$('text.name').css('font-size',this.value+'pt');"></td>
               </tr><tr>
                  <td align='left'>Label color: </td>
                  <td><input id='lcolor' class='color' size='6' value='000' onkeyup="$('text.name').css('fill','#'+this.value);"></td>
               </tr><tr>
                  <td align='left'>Label font: </td>
                  <td><select id='lfont' style='width: 80px;' onchange="$('text.name').css('font-family',this.value);">
                     <option value='Verdana, Geneva, sans-serif'>Verdana</option>
                     <option value='Arial, Helvetica, sans-serif'>Arial</option>
                     <option value='"Times New Roman",Georgia,Serif'>Times New Roman</option>
                     <option value='"Courier New", Courier, monospace'>Courier New</option>
                  </select></td>
               </tr><tr>
                  <td align='left'>Bootstrap size: </td>
                  <td><input id='bsize' size='2' value='6' onkeyup="$('text.bootstrap').css('font-size',this.value+'pt');"></td>
               </tr><tr>
                  <td align='left'>Bootstrap color: </td>
                  <td><input id='bcolor' class='color' size='6' value='000' onkeyup="$('text.bootstrap').css('fill','#'+this.value);"></td>
               </tr><tr>
                  <td align='left'>Bootstrap font: </td>
                  <td><select id='bfont' style='width: 80px;' onchange="$('text.bootstrap').css('font-family',this.value);">
                     <option value='Verdana, Geneva, sans-serif'>Verdana</option>
                     <option value='Arial, Helvetica, sans-serif'>Arial</option>
                     <option value='"Times New Roman",Georgia,Serif'>Times New Roman</option>
                     <option value='"Courier New", Courier, monospace'>Courier New</option>
                  </select></td>
               </tr>
            </table><table style='display: inline;width:256px;'>
               <tr>
                  <td align='left'>Branch size: </td>
                  <td><input id='branchsize' size='2' onkeyup="SetBranches();" value='1.5'></td>
               </tr><tr>
                  <td align='left'>Branch color: </td>
                  <td><input id='branchcolor' class='color' size='6' onkeyup="SetBranches();" value='C0C0C0'></td>
               </tr><tr>
                  <td align='left'>Node size: </td>
                  <td><input id='nodesize' size='2' onkeyup="SetNodes();" value='3'></td>
               </tr><tr>
                  <td align='left'>Node color: </td>
                  <td><input id='nodecolor' class='color' size='6' onkeyup="SetNodes();" value='00B'></td>
               </tr>
            </table><table style='display: inline;width:256px;'>
               <tr>
<!--                  <td align='left'>Image sizes: </td>
                  <td><input id='imagesize' size='2' onkeyup='SetImages();' value='200'></td>
               </tr><tr>
-->
                  <td colspan='2'><input type='button' onclick='hidebootstrap(this);' value='Hide Bootstraps'></td>
               </tr><tr>
                  <td colspan='2'><input id='nodebutt' type='button' onclick='hidenodes(this);' value='Show Nodes'></td>
               </tr><tr>
                  <td colspan='2'><input type='button' onclick='hidelabels(this);' value='Hide Labels'></td>
               </tr><tr>
                  <td colspan='2' align='left'>Selective Label coloring:</td>
               </tr><tr>
                  <td><input id='findlabels' size='10' onkeyup="SelectLabels();" value=''></td>
                  <td><input id='findcolor' class='color' size='6' value='000' onkeyup="Colorselected();"></td>
               </tr>
            </table><br>
                  <input type='button' id='submit' onclick='ShowNewick();' value='Adjust Tree'>
                  <input type='button' onclick='ResetUI();' value='Reset Tree'><br>
                  <!--<button onclick="svgdownload('svg');">Save Workspace</button><br>-->
                  <!--Load Workspace: <input type="file" id="files" onchange='LoadSVG(this)' value='test'/>-->
            </form>
         </td>
<!-- USER INTERACTION FORM (END) -->
      </tr>
   </table>
<!-- IMAGE COMPARE - ->
  <div style='margin: auto;' class='noPrint'>
	 <script>
	   for(var i=1;i<=7;i++){
		  document.write("<img id='c"+i+"' src='/services/MHCcluster-2.0/bin/none.png' alt='compare"+i+"' class='compareImg'>");
		}
	 </script>
  </div>
<!-- IMAGE COMPARE (END) -->
</div>

<script type="text/javascript">
   window.imageDir = '<?php echo $imageDir; ?>';
   ShowNewick();
   d3.selectAll('circle.node').style('display', 'None');
   $(document).ready(function(){
      d3.selectAll('circle.node').style('display', 'None');
   });
</script>

<?php if (count($_POST)==0 and count($_GET)==0){ ?>
<br><hr>
<h3>UPLOAD NEWICK</h3>
Load your own newick file by copy pasting it into the following text area:<br>
<input type='button' value='Load Newick' onclick='ShowNewick2();'><br>
<textarea id='newick_text'></textarea>
<?php } ?>

<hr><!-- REFERENCES -->
<h3>REFERENCES</h3>
<p class="bulk">This TreeViewer is a modified version of the <a href='http://www.cbs.dtu.dk/services/MHCcluster-2.0/abstract.php'>MHCcluster TreeViewer</a> and is used to view phylogenetic trees.</p>

<hr><!-- CITATIONS -->

<h3>CITATIONS</h3>
<p>For publication of results, please cite:</p>
<ul>
<li>MHCcluster, a method for functional clustering of MHC molecules.<br>
Martin Thomsen, Claus Lundegaard, Soren Buus, Michael Rasmussen, Ole Lund and Morten Nielsen<br>
Immunogenetics. 2013 Jun 18. [Epub ahead of print]</li>
</ul>

<!-- END OF CONTENT -->
<?php
$CGE->Javascripts($service, $version, "/home/data1/services/"); // Printing Applet required Javascript!!
$CGE->ReadTypeCheck(); // Printing the javescript required to test if the correct number of files were uploaded!!

// PIWIK WEBSITE TRAFIC TRACKING
// The Number in the following parenteses should match the PIWIK Website ID.
// To get the ID go to http://cge.cbs.dtu.dk/piwik/, login and then click on add new website in the bottom.
// Now the ID of all websites appear, and if this website is not in the list, then add it, and note down the ID, and use it below.
$CGE->Piwik(14);

# STANDARD FOOTER
# First a simple headline like: "Support"
# Then a list of emails like this: "('Scientific problems','foo','foo@cbs.dtu.dk'),('Technical problems','bar','bar@cbs.dtu.dk')"
$CGE->standard_foot("Support","('Scientific problems','Martin Thomsen','mcft'),('Technical problems','CGE Support','cgehelp')");
?>