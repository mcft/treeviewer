#!/usr/local/python/bin/python2.7
import os, sys, glob, shutil
from zipfile import ZipFile

# This function creates a zipfile located in zipFilePath with the files in the file list
def createZipDir(zipFilePath, *fileList): # fileList can be both a comma separated list or an array
   try:
      if isinstance(fileList, (list, tuple)): #unfolding list of list or tuple
         if len(fileList) == 1:
            if isinstance(fileList[0], (list, tuple)): fileList = fileList[0]
      if isinstance(fileList, str): fileList = [fileList] #converting string to iterable list
      if fileList:
         zipDir = '/'.join(zipFilePath.split('/')[:-1])
         if zipDir:
            zipDir+='/'
            os.chdir(zipDir)
         with ZipFile(zipFilePath, 'w') as zf:
            for curFile, dst in fileList:
               if os.getcwd() != dst and dst:
                  os.chdir(dst)
               print('Adding %s (src: %s)'%(curFile, dst+curFile))
               zf.write(curFile)
      else:
         print('ERROR: No Files in list!',zipFilePath+' was not created!')
   except Exception, e:
      print('ERROR: Could not create zip dir! argtype: '+str(type(fileList)), "FileList: "+ str(fileList), "Errormessage: "+ str(e))

# Direct execution of this wrapper.
if __name__ == "__main__":
   mainDir = os.path.abspath('/'.join(__file__.split('/')[:-1]))
   if mainDir: mainDir+='/'
   fileList = [('treeviewer.html',            mainDir), # (local_path, external_path)
               ('js/d3.v2.js',                mainDir),
               ('js/jquery-ui.min.js',        mainDir),
               ('js/jquery.min.js',           mainDir),
               ('js/newick.js',               mainDir),
               ('js/newick2svg.js',           mainDir),
               ('js/tree-ui.js',              mainDir),
               ('js/svg2pdf.js',              mainDir),
               ('js/jscolor/arrow.gif',       mainDir),
               ('js/jscolor/cross.gif',       mainDir),
               ('js/jscolor/hs.png',          mainDir),
               ('js/jscolor/hv.png',          mainDir),
               ('js/jscolor/jscolor.js',      mainDir),
               ('css/jquery.contextMenu.css', mainDir),
               ('css/style.css',              mainDir),
               ('bin/none.png',               mainDir)]
   alleles = []
   if len(sys.argv) > 1:
      tmpdir = mainDir+sys.argv[1]+'/' #+'tmp/'
      if os.path.exists(tmpdir):
         os.chdir(tmpdir)
         alleles = glob.glob("bin/*.png")
         length = len(alleles)
         if length <= 0: sys.exit("NO FILES WERE FOUND! (%s)"%(tmpdir+"bin/*.png"))
         else:
            alleles = zip(alleles, [tmpdir,]*length)
            fileList.extend(alleles)
            fileList.append(('bin/tree.newick', tmpdir))
            fileList.append(('bin/alleles.pred', tmpdir))
            zipFilePath = tmpdir+'TreeViewer.zip'
      else: sys.exit("NO SUCH DIRECTORY: %s!"%(tmpdir))
   if not alleles:
      fileList.extend([('bin/tree.newick',   mainDir),
                       ('bin/HLA-A0101.png', mainDir),
                       ('bin/HLA-A0201.png', mainDir),
                       ('bin/HLA-A0204.png', mainDir),
                       ('bin/HLA-A0206.png', mainDir),
                       ('bin/HLA-A0301.png', mainDir),
                       ('bin/HLA-A1101.png', mainDir),
                       ('bin/HLA-A2301.png', mainDir),
                       ('bin/HLA-A2402.png', mainDir),
                       ('bin/HLA-A2601.png', mainDir),
                       ('bin/HLA-A2902.png', mainDir),
                       ('bin/HLA-A3001.png', mainDir),
                       ('bin/HLA-A3002.png', mainDir)])
      zipFilePath = mainDir+'TreeViewer.zip'
   
   if len(fileList) > 0:
      # Creating gzipped dir
      createZipDir(zipFilePath, fileList)
      print("ZipDir created: "+zipFilePath+"\n")   