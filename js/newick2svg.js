/* NAME:        newick2svg.js - SVG Tree Builder (TreeViewer)
 * AUTHOR:      Martin Thomsen
 * AFFILIATION:
      Center for Biological Sequence Analysis, Technical University of Denmark
 * CITATION:
      M.Thomsen, O.Lund, C.Lundegaard and M. Nielsen, "MHCcluster, a method for
      functional clustering of MHC molecules." Immunogenetics, 2013, ED: p.
      XXX-YYY (Publication Underway).
 * DESCRIPTION:
      Translates a newick formatted tree file into a visual interpretation of
      the tree in the SVG format, and applies user interaction and image
      previews features.
      This script was made for a project to improve the phenetic tree of the
      MHCcluster online service tool. The improvements includes the
      visualisation of the MHC allele binding motifs and making the tree
      interactive.
      This script implements methods for direct dynamic manipulation of the
      Scalable Vector Graphic (SVG) in the Document Object Model (DOM).
 * DEPENDENCIES:
      1. d3.v2.js     - Data Driven Documents
      2. jquery.js    - jQuery
      3. jquery-ui.js - jQuery user interaction
      4. newick.js    - Newick parsing script to a Java Script Object
      5. tree-ui.js   - Tree user interaction & motif previews
      6. style.css    - Style sheet
 * USAGE: 
      Newick2Tree(path, dim, span, rotation, margin, label, bootstrap);
      Newick2Tree(virtualPathToNewickFile, [width,height], degrees, degrees,
                  margin, [size, color, font], [size, color, font]);
 * EXAMPLE: 
      Newick2Tree("path/to/newick.ext", [800,500], 360, 0, 0, ['9', '#555',
                  'Verdana, Geneva, sans-serif'], ['8', '#CCC', 'Verdana,
                  Geneva, sans-serif']);
 */

// Define Globals
window.svgMargin = 10;
window.scale = 1;
window.textMarginBox = [0,0,0,0];
window.label = ['8pt','#000','Verdana, Geneva, sans-serif'];
window.bootstrap = ['8pt','#000','Verdana, Geneva, sans-serif'];

$(document).ready(function(){
   // Getting the offset for the SVG
   window.SVGoffset = $('svg').offset();
});

function UrlExists(url){
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}

function Newick2Tree(path, dim, span, rotation, margin, label, bootstrap, activatenodes_){  //__MAIN__
  //This function Extract the data from a newick file and Creates a Tree
  activatenodes_ = typeof(activatenodes_) != "undefined" ? activatenodes_: true
  // CHECK IF PATH EXISTS
  if(!UrlExists(path)){
    alert('The requested file does not exist!');
    return false; // END FUNCTION
  }
  
  // SETTING GLOBALS
  if(label){
    if(parseInt(label[0]) > 0){ window.label[0]=label[0]+'pt'; }else{ window.label[0]='8pt'; }
    if(/(^[0-9A-F]{6}$)|(^[0-9A-F]{3}$)/i.test( label[1] )){ window.label[1]='#'+label[1]; }else{ window.label[1]='#C0C0C0'; }
    if(label[2].length > 12){ window.label[2]=label[2]; }else{ window.label[2]='Verdana, Geneva, sans-serif'; }
  }
  if(bootstrap){
    if(parseInt(bootstrap[0]) > 0){ window.bootstrap[0]=bootstrap[0]+'pt'; }else{ window.bootstrap[0]='8pt'; }
    if(/(^[0-9A-F]{6}$)|(^[0-9A-F]{3}$)/i.test( bootstrap[1] )){ window.bootstrap[1]='#'+bootstrap[1]; }else{ window.bootstrap[1]='#C0C0C0'; }
    if(bootstrap[2].length > 12){ window.bootstrap[2]=bootstrap[2]; }else{ window.bootstrap[2]='Verdana, Geneva, sans-serif'; }
  }
  
  // Clear and Resize the SVG Platform
  clearSVG();
  window.wrap = d3.select("svg")
    .attr("width", dim[0]+'px')
    .attr("height", dim[1]+'px');

  //DrawBox([0, 0, dim[0], dim[1]])
  
  // ADDING USER REQUENSTED MARGIN
  if(parseInt(margin) > 0 || parseInt(margin) < 0 ){ window.svgMargin = 10 + margin; }else{ window.svgMargin = 10; }
  
  d3.text(path, function(text) {
    // Parse newick file to object x
    var x = newick.parse(text);

    // Set Tree Parameters
    x.span = span;
    x.direction = rotation;
    x.length = 0;
    window.textMarginBox = [0,0,0,0]; //Resetting text box margins
    
    // Compute Spacial distribution of the Tree
    determinBrachSize(x);
    calcBranchDirections(x);
    var box = calcBranchLength(x, 0, 0);
    
    // DETERMINE SCALING AND CENTERING OF THE FINAL TREE
    var tmp = calculateScaleCenter(box, dim);
    window.scale = tmp[0]; 
    x.x = tmp[1], x.y = tmp[2]; // Setting the Tree center coordinates
    
    // DRAW THE TREE
    drawTree(x);
    
    // Make Allele List
    //MakeAllelist();
    
    //Activate Node events
    if(activatenodes_){
       ActivateNodes(); // tree-ui.js function
    }
  });
  return true;
};

function Newick2Tree2(text, dim, span, rotation, margin, label, bootstrap, activatenodes_){  //__MAIN__
  //This function Extract the data from a newick file and Creates a Tree
  activatenodes_ = typeof(activatenodes_) != "undefined" ? activatenodes_: true
  
  // SETTING GLOBALS
  if(label){
    if(parseInt(label[0]) > 0){ window.label[0]=label[0]+'pt'; }else{ window.label[0]='8pt'; }
    if(/(^[0-9A-F]{6}$)|(^[0-9A-F]{3}$)/i.test( label[1] )){ window.label[1]='#'+label[1]; }else{ window.label[1]='#C0C0C0'; }
    if(label[2].length > 12){ window.label[2]=label[2]; }else{ window.label[2]='Verdana, Geneva, sans-serif'; }
  }
  if(bootstrap){
    if(parseInt(bootstrap[0]) > 0){ window.bootstrap[0]=bootstrap[0]+'pt'; }else{ window.bootstrap[0]='8pt'; }
    if(/(^[0-9A-F]{6}$)|(^[0-9A-F]{3}$)/i.test( bootstrap[1] )){ window.bootstrap[1]='#'+bootstrap[1]; }else{ window.bootstrap[1]='#C0C0C0'; }
    if(bootstrap[2].length > 12){ window.bootstrap[2]=bootstrap[2]; }else{ window.bootstrap[2]='Verdana, Geneva, sans-serif'; }
  }
  
  // Clear and Resize the SVG Platform
  clearSVG();
  window.wrap = d3.select("svg")
    .attr("width", dim[0]+'px')
    .attr("height", dim[1]+'px');

  //DrawBox([0, 0, dim[0], dim[1]])
  
  // ADDING USER REQUENSTED MARGIN
  if(parseInt(margin) > 0 || parseInt(margin) < 0 ){ window.svgMargin = 10 + margin; }else{ window.svgMargin = 10; }
  
  // Parse newick file to object x
  var x = newick.parse(text);

  // Set Tree Parameters
  x.span = span;
  x.direction = rotation;
  x.length = 0;
  window.textMarginBox = [0,0,0,0]; //Resetting text box margins
  
  // Compute Spacial distribution of the Tree
  determinBrachSize(x);
  calcBranchDirections(x);
  var box = calcBranchLength(x, 0, 0);
  
  // DETERMINE SCALING AND CENTERING OF THE FINAL TREE
  var tmp = calculateScaleCenter(box, dim);
  window.scale = tmp[0]; 
  x.x = tmp[1], x.y = tmp[2]; // Setting the Tree center coordinates
  
  // DRAW THE TREE
  drawTree(x);
  
  // Make Allele List
  //MakeAllelist();
  
  //Activate Node events
  if(activatenodes_){
     ActivateNodes(); // tree-ui.js function
  }
  return true;
};

function determinBrachSize(n){
   //This function calculates and sets the size of the node and its subnodes
   var size = typeof n.length !== 'undefined' ? sizefct(n.length) : 0;
   if(n.branchset){
	   n.branchset.forEach(function(subbranch){
		   size += determinBrachSize(subbranch);
      });
	};
   n.size = size;
	return size;
};

function sizefct(v){
   //This function calculates a size from the given value
   if(v!=0){
    return 1;//(Math.abs(v)+1)*Math.pow(v+1,1.5);
   }else{
    return 1;
   }
};

function DrawBox(b){
  //This function Draws the box [x0, y0, x, y]
  wrap.append('path')
    .attr('class', 'path')
    .attr('d', 'M'+b[0]+','+b[1] +'L'+b[2]+','+b[1] +'L'+b[2]+','+b[3] +'L'+b[0]+','+b[3] +'L'+b[0]+','+b[0]);
};

function clearSVG(){
  //This function Removes the path and text objects from the DOM, rendering the SVG clear.
  d3.selectAll("path").remove();
  d3.selectAll("text").remove();
  d3.selectAll("circle").remove();
};

function calcBranchDirections(n){
  // This function calculates the span and direction for each branch
   var sumChildSpan = 0;
   var nsize = (n.size - sizefct(n.length));
   var nspan = n.span;
   var ndirection = n.direction;
   if(n.branchset){
	   n.branchset.forEach(function(c){
         c.span = nspan / nsize * c.size;
         c.direction = ndirection + c.span/2 + sumChildSpan - nspan/2;
         sumChildSpan += c.span;
         // Continue down the brach
		   calcBranchDirections(c);
      });
   }
};

function calcBranchLength(n, x0, y0){
  // This function Computes the branch lengths and updates the Tree box coordinates
  var box = [0, 0, 0, 0]; // box coordinates: x0, y0, x, y
  if(n.branchset){
    n.branchset.forEach(function(c){
		//Calculate the branch x, y length 
      var dir = c.direction/180*Math.PI; // convert degrees to radians
      c.x = c.length * Math.cos(dir), c.y = c.length * Math.sin(dir);

      //Compute the branch x, y coordinate
      cx = parseFloat(x0) + c.x, cy = parseFloat(y0) + c.y;
      //Continue down the branch
      var cbox = calcBranchLength(c, cx, cy);

      // Update the Tree box Coordinates
      if(box[0] > cx){ box[0] = cx; } //lowest x value
      if(box[0] > cbox[0]){ box[0] = cbox[0]; }
      if(box[1] > cy){ box[1] = cy; } //lowest y value
      if(box[1] > cbox[1]){ box[1] = cbox[1]; }
      if(box[2] < cx){ box[2] = cx; } //highest x value
      if(box[2] < cbox[2]){ box[2] = cbox[2]; }
      if(box[3] < cy){ box[3] = cy; } //highest y value
      if(box[3] < cbox[3]){ box[3] = cbox[3]; }

      //DRAW BOOTSTRAP VALUES
    });
  }else{
    //Annotate IDs
    n.id = n.name.replace(/[^a-zA-Z0-9\_\-]/g,'');

    // Adding the name label
    DrawText(n);

    // Computing the text length
    n.textlength = document.getElementById('text_'+n.id).getComputedTextLength(); // text length
    
    // Compute text X and Y coordinates
    var nodeSize = 3;
    if ($('#nodesize').length > 0) {
      nodeSize = parseFloat(document.getElementById('nodesize').value); // node color
    }
    var dir = n.direction/180*Math.PI; // convert degrees to radians
    tx = (n.textlength+nodeSize+2) * Math.cos(dir), ty = (n.textlength+nodeSize+2) * Math.sin(dir);

    // Update textMarginBox
    if(textMarginBox[0] > tx){ textMarginBox[0] = tx; } //lowest x value
    if(textMarginBox[1] > ty){ textMarginBox[1] = ty; } //lowest y value
    if(textMarginBox[2] < tx){ textMarginBox[2] = tx; } //highest x value
    if(textMarginBox[3] < ty){ textMarginBox[3] = ty; } //highest y value
  };
  return box;
};

function calculateScaleCenter(box, dim){
   //This function Calculates the scaling needed and determines the center position
   var tmp = [((dim[0]-svgMargin*2)-(textMarginBox[2]-textMarginBox[0]))/(box[2]-box[0]), ((dim[1]-svgMargin*2)-(textMarginBox[3]-textMarginBox[1]))/(box[3]-box[1])];
   var scale = (tmp[0]<tmp[1]?tmp[0]:tmp[1]); 
   var center = [(dim[0]-(box[2]+box[0])*scale)/2-(textMarginBox[0]+textMarginBox[2])/2, (dim[1]-(box[3]+box[1])*scale)/2-(textMarginBox[1]+textMarginBox[3])/2]
   return [scale, center[0], center[1]];
};

function drawTree(n){
   // DRAW THE BRANCHES
   if(n.branchset){
      n.branchset.forEach(function(c){
         // RESCALE AND POSITION BRANCH
         c.x = n.x + c.x * scale, c.y = n.y + c.y * scale;
         //Annotate IDs
         n.id = 'bs_'+ Math.floor(c.x)+Math.floor(c.y);
         // DRAW BRANCH
	      drawBranch(n.x, n.y, c.x, c.y);
			// CONTINUE to the next node on the branch untill the end
			drawTree(c);
		});
   }
	//Draw the leaf (name tags)
	if(n.name){ drawLeaf(n); };
};

function drawLeaf(n){
  //This function draws the bootstrap values or repositions the node labels to their correct position
  if(n.branchset){
    var ndir = (n.direction)%360;
    var dir = ndir/180*Math.PI; // convert degrees to radians
    var x = n.x + 5 * Math.cos(dir), y = n.y + 5 * Math.sin(dir);
    var id = 'bs'+Math.floor(x)+Math.floor(y);
    
    // Draw bootstrap values
    wrap.append('text')
      .attr('class', 'bootstrap')
      .attr('id', id)
      .style("font-size", window.bootstrap[0])
      .style("fill", window.bootstrap[1])
      .style("font-family", window.bootstrap[2])
      .text(n.name);
    
    var tl = document.getElementById(id).getComputedTextLength()/2; // text half length
    var td = n.length*scale/2+5; // text displacement
    var tmp = (ndir < 90 ?
                (ndir < -90 ?
                  (ndir < -270 ?
                    [0,-tl-td] :
                    [180,-tl+td]
                  ) :
                  [0,-tl-td]
                ) :
                (ndir > 270 ?
                 [0,-tl-td] :
                 [-180,-tl+td]
                )
              );
    var textdir = ndir+tmp[0];
    td = tmp[1]; // updating text displacement

    d3.select("#"+id)
      .attr('transform', 'rotate('+textdir+', '+x+', '+y+')translate('+td+', -1)')
      .attr("x", x).attr("y", y);
  }else{
     // Getting node text length and direction
     var ts = 3+1;
     if ($('#nodesize').length > 0) {
       ts = parseFloat(document.getElementById('nodesize').value)+1; // text space
     }
     var tl = n.textlength;
     var ndir = (n.direction)%360;

     // Adjusting text direction and position, to make it easy to read (left to right reading...)
     var tmp = (ndir < 90 ?
                 (ndir < -90 ?
                   (ndir < -270 ?
                     [0,ts] :
                     [180,-tl-ts]
                   ) :
                   [0,ts]
                 ) :
                 (ndir > 270 ?
                   [0,ts] :
                   [-180,-tl-ts]
                 )
               );
     var textdir = ndir+tmp[0];
     tl = tmp[1];

     // Reposition Text Label
     var data = {}; // create new dataset
     d3.select("#text_"+n.id).each(function(d) { for(var k in d) { data[k] = d[k]; } }); // retrieve privious data
     data['rot'] = textdir+', '+n.x+', '+n.y;// Set textdir
     data['xo'] = tl; // Set x offset
     data['yo'] = 4;  // Set y offset
     d3.select("#text_"+n.id).data([data]).attr('transform', 'rotate('+textdir+', '+n.x+', '+n.y+')translate('+tl+', 4)')
       .attr("x", n.x).attr("y", n.y);

      // DRAW NODE
      drawNode(n, n.x, n.y);
  }
};

function drawBranch(x0, y0, x, y){
  // This function Draws the branch by appending a path to the SVG Object
  wrap.append('path')
    .attr('class', 'path')
    .attr('d', 'M'+x0+','+y0+'L'+x+','+y);
};

function drawNode(n, x, y){
  // This function Draws the node by appending a circle to the SVG Object, and adding events
  wrap.append('circle')
    .attr('class', 'node preview')
    .attr("cx", x)
    .attr("cy", y)
    .attr("r", 3)
    .attr("id", "node_"+n.id)
    ;
};

function DrawText(n){
  wrap.append('text')
    .attr('class', 'name preview')
    .attr('id', 'text_'+n.id)
    .data([{ 'x': 0, 'y': 0}])             // x and y offsets, used for dragging
    .style("font-size", window.label[0])
    .style("fill", window.label[1])
    .style("font-family", window.label[2])
    .text(n.name.replace('_',':'))
    ;
};
