/* NAME:    tree-ui.js - Tree Viewer User Interaction (TreeViewer)
 * AUTHOR:  Martin Thomsen
 * AFF:     Center for Biological Sequence Analysis, Technical University of Denmark
 * CONTACT: mcft AT cbs DOT dtu DOT dk
 * CITE:    M.Thomsen, O.Lund, C.Lundegaard and M. Nielsen, "MHCcluster, a
            method for functional clustering of MHC molecules." Immunogenetics,
            2013, ED: p. XXX-YYY. (Publication Underway)
 * DESC:    This library extends the tree builder script 'newick2svg.js' by
            adding user interaction features to the Scalable Vector Grafics
            (SVG).
 * FEATS:   Visual modifications including colors, line widths, fonts etc.
            Image Previews (shows MHC motifs, ids matchs filenames in bin dir)
            Compared Bar
            Image/Label rearrangement
            Slide-down menu for labels
 * DEPENDS: d3.v2.js, jquery.js, jquery-ui.js, newick.js, newick2svg.js
 */

/* TODO:
 * Test TreeViewer On Windows, with IE9, FF, Crome
 */
var imageSize = 200;
var nodeSize = 3;

function compareImg(slot, path){
  // CHANGING THE IMAGE OF THE COMPARE SLOT (slot)
  path = (path) ? path : "bin/none.png";
  document.getElementById(slot).src = path;
}

function ResetCompares(){
  // RESETTING ALL 7 COMPARE SLOTS
  for(var i=1; i<=7; i++){
    compareImg('c'+i);
  }
}

function RmImg(previewNo){
   // REMOVING IMAGE
   d3.select("#"+previewNo).remove();
   // UPADTING THE ALLELE LIST
   $('#allelelist')[0].options[parseInt(previewNo.replace('preview',''))].selected=false;
   // UPADTING THE COMPARE LIST
   CompareSelected($('#allelelist')[0].options);
}

function CompareSelected(opts){
  // RESETTING THE COMPARE SLOTS
  ResetCompares()
  // CHECKING FOR SELECTED ALLELES
  for(var i=0, j=1, l=opts.length; i<l; i++){
    if(opts[i].selected){ // Compare Allele
      compareImg('c'+j, getImgPath(opts[i]));
      j++;
      if(j>7) break;
    }
  }
}

// SVG Drag function: the objects needs a data attribute with x, y coordinates: .data([{'x':0,'y':0}])
var drag = d3.behavior.drag()
   .on("drag", function(d,i) {
      d.x += d3.event.dx;
      d.y += d3.event.dy;
      d3.select(this).attr("transform", function(d,i){
        if (d.hasOwnProperty('rot')){
          var V = parseFloat(d.rot.split(',')[0])/180*Math.PI;
          var x = d.x*Math.cos(V)+d.y*Math.sin(V);
          var y = d.y*Math.cos(V)-d.x*Math.sin(V);
          return 'rotate('+d.rot+')translate('+[x,y]+')';
        }
        else if (d.hasOwnProperty('ox')){ return 'translate('+[d.x+d.xo,d.y+d.yo]+')'; }
        else { return 'translate('+[d.x,d.y]+')'; }
      });
   })
   .on('dragend', function(){ mouseUp(); }) // Updates the releaes of the left mouse button
   ;

// SVG Arrange function: this function moves the current element to the bottom of the svg, thus it is printed on top.
function move2front(e) { e.parentNode.appendChild(e); }

function PreviewSelected(selector){
  // GETTING THE OPTIONS
  var opts = selector.options;
  // SHOWING THE SELECTED ALLELES
  for(var i=0, l=opts.length; i<l; i++){
    // CHECKING IF THE PREVIEW EXISTS ALREADY
    var previewExist, preview = document.getElementById('preview'+i);
    if (typeof(preview) != 'undefined' && preview != null){ previewExist = true; }else{ previewExist = false; }
    if(opts[i].selected){ // If allele was selected
      if(!previewExist){ // and if its preview is non-existing
        // ADD PREVIEW
        path = getImgPath(opts[i]);
        id = opts[i].id.replace('option_','node_').replace(/[^a-zA-Z0-9\_\-]/g,'');
        offset = $("#"+id).offset();
        d3.select("svg").append("svg:image")
          .attr("xlink:href", path)                      // src of image (required)
          .attr("width", imageSize)                      // width of image (required)
          .attr("height", imageSize)                     // width of image (required)
          .attr("x", (offset.left + nodeSize - $('#svg').offset().left))    // X coordinate of image
          .attr("y", (offset.top  + nodeSize - $('#svg').offset().top -25)) // Y coordinate of image
          .data([{ 'x': 0, 'y': 0}])                     // x and y offsets, used for dragging
          .call(drag)                                    // ADD dragable feature
          .attr("id", 'preview'+i)                       // needed for the RmImg fct.
          .on("click", function(){ move2front(this); })  // ADD label/image arrage feature
          .on("dblclick", function(){ RmImg(this.id); }) // ADD remove feature
          ;
      }
    }else{ // if the allele isn't selected
      if(previewExist){ // but the preview does exists
        // MARK ALLELE AS SELECTED ON ALLELELIST
        opts[i].selected = 'Selected';
      }
    }
  }
  // UPADTING THE COMPARE LIST
  CompareSelected(opts);
}

function getImgPath(elem, e){
  // DEFINING IMG PATH
  var path;
  if(elem.tagName == 'circle'){
    path = imageDir +'/'+ document.getElementById(elem.id.replace('node_','text_')).textContent.replace(/[^a-zA-Z0-9\_\-\.]/g,'') +".png";
    offset = $('#'+elem.id).offset();
    //                node-pos    -cursorPos  node-adj. offset  imgHeightAdj.
    window.xOffset =  offset.left - e.pageX + nodeSize*2 +5;
    window.yOffset =  offset.top  - e.pageY + nodeSize  -15 -300/800*imageSize;
  }else if(elem.tagName == 'text'){
    path = imageDir +'/'+ elem.textContent.replace(/[^a-zA-Z0-9\_\-\.]/g,'') +".png";
    //              offset imgHeightAdj.
    window.xOffset =  5;
    window.yOffset =  -15 -300/800*imageSize;
  }else if(elem.tagName == 'OPTION'){
    path = imageDir +'/'+ elem.value +".png";
    offset = $(document.getElementById(elem.id.replace('option_','node_'))).offset();
    if(e){
      //               node-pos    -cursorPos  node-adj offset.
      window.xOffset = offset.left - e.pageX + nodeSize;
      window.yOffset = offset.top  - e.pageY + nodeSize;
    }
  }
  return path;
}

function addPreview(elements, click){
  // HANDLING OPTIONAL ARGUMENTS
  click = (click) ? true : false;
  // ADDING CLICK EVENT
  elements
    .mousedown( function(e) {
      if(click){
        var rightclick;
	     if (!e) e = window.event;
	     if (e.which) rightclick = (e.which == 3);
	     else if (e.button) rightclick = (e.button == 2);
        if(rightclick){ // --> SHOW MENU
          path = getImgPath(this, e);
          var menu = "<ul id='menu' class='menu'>";
          for(var i=1;i<=7;i++){
              menu += "<li><p onclick='compareImg(&quot;c"+i+"&quot;, &quot;"+
                                 path+"&quot;);'>Show in slot "+i+"</a></li>";
          }
          menu += "</ul>";
          $("body").append(menu); //ADD MENU
          $("#menu")
            .css("top",(e.pageY - 25 ) + "px")
            .css("left",(e.pageX - 100 ) + "px")
            .slideDown("fast")
            .click(
              function() { // ADDING SUBHOVER Click Event
                //When the user clicks on an option, the box disapears 
                $("#menu").slideUp('slow').remove();
              }
            ).hover(
              function() { // ADDING SUBHOVER Hover Event
              }, function(){ // Hover Out
                //When the mouse hovers out of the subnav, move it back up  
                $("#menu").slideUp('slow').remove(); 
              }
            );
        }
      mouseDown(e);
      e.stopImmediatePropagation();
      }
    })
    .mouseup(function(e){
      if(click){
         mouseUp(e);
         e.stopImmediatePropagation();
      }
    })
    .mouseover( function(e){ // ADDING HOVER EVENT - SHOW IMG PREVIEW
        if(!leftButtonDown){ // No Drag Event Occuring
          ShowPreview(getImgPath(this, e), (e.pageX + xOffset), (e.pageY + yOffset));
        }
        e.stopImmediatePropagation();
      })
    .mouseout( function(){ $("img.preview").remove(); }) // REMOVE IMG PREVIEW
    ;
}

function ShowPreview(path, x, y){
   var options = $('option.preview');
   var allele =  path.split('/').slice(-1)[0].split('.').slice(0, -1).join('.');
   for(var i=0; i<options.length; i++){
      if(options[i].value == allele && !options[i].selected){ // Skipping preview of selected alleles
         if(!$("img.preview").length > 0){
            $("body").append("<img class='preview' alt='Image preview' />");
         }
         $("img.preview")
            .attr('width', imageSize +'px')
            .attr('src', path)
            .css("top", y+"px")
            .css("left", x+"px")
            .fadeIn("fast")
            ;
      }
   }
}

this.imagePreview = function(){
  // Initialising variables
  xOffset = 0;
  yOffset = 0;
  curSlot = 0;
  // ADD PREVIEW TO TAGS
  addPreview($("circle.preview"), true);
  addPreview($("text.preview"), true);
  //addPreview($("option.preview"));
}

// starting the script on page load
//$(document).ready(function(){
//	imagePreview();
//});

// TRACKING LEFT MOUSEBUTTON -> to remove hover effect when dragging objects...
window.leftButtonDown = false;
function mouseUp(e){ // Left mouse button was released, clear flag
   if(!e){ window.leftButtonDown = false; }
   else if(e.which === 1) window.leftButtonDown = false;
}
function mouseDown(e){ // Left mouse button was pressed, set flag
   if(e.which === 1) window.leftButtonDown = true;
}

String.prototype.format = function() {
  // This prototype enables format printing: "{0} me ${1}".format('lend', 1.853) ==> 'lend me $1.85'
  var args = arguments;
  return this.replace(/{(\d+)}/g, function(match, number) { 
    return typeof args[number] != 'undefined'
      ? (isNaN(args[number]) ? args[number] : Math.round(args[number]*100)/100)
      : match
    ;
  });
};

function hidebootstrap(button){
  if (button.value == 'Hide Bootstraps'){
    button.value = 'Show Bootstraps';
    d3.selectAll('text.bootstrap').style('display', 'None');
  }else{
    button.value = 'Hide Bootstraps';
    d3.selectAll('text.bootstrap').style('display', 'Inline');
  }
};

function hidenodes(button){
  if (button.value == 'Hide Nodes'){
    button.value = 'Show Nodes';
    d3.selectAll('circle.node').style('display', 'None');
  }else{
    button.value = 'Hide Nodes';
    d3.selectAll('circle.node').style('display', 'Inline');
  }
};

function hidelabels(button){
  if (button.value == 'Hide Labels'){
    button.value = 'Show Labels';
    d3.selectAll('text.name').style('display', 'None');
  }else{
    button.value = 'Hide Labels';
    d3.selectAll('text.name').style('display', 'Inline');
  }
};

function ActivateNodes(){
  //imagePreview(); // Adding previews to all nodes
  SetNodes();     // SET NODE SIZE, COLOR AND HOVER EVENT
  SetBranches();  // SET BRANCH SIZE AND COLOR
  SetLabel();     // SET LABEL INTERACTION
};

function ih(h){return (255 - parseInt(h, 16)).toString(16);} // inverse_hex_number(hex_number)
function ss(s,f,t){return s.substring(f,t);} // substring(string, from, to)

function SetNodes(){ // SET NODE SIZE, COLOR AND HOVER EVENT
  var nc = "000000";
  var ns = 3;
  if ($('#nodecolor').length > 0 && $('#nodesize').length > 0) {
     nc = document.getElementById('nodecolor').value; // node color
     ns = parseFloat(document.getElementById('nodesize').value); // node color
  }
  if(nc && ns){
    if(nc.length==3){ nc = ss(nc,0,1)+ss(nc,0,1)+ss(nc,1,2)+ss(nc,1,2)+ss(nc,2,3)+ss(nc,2,3); } // transform 1 digit (rgb) hex to 2 digit (rgb) hex
    ic = ih(ss(nc,0,2))+ih(ss(nc,2,4))+ih(ss(nc,4,6)); // inverse node color
    $('circle')
      .attr('r', ns)
      .css('fill','#'+nc)
      .on("mouseover", function(){ $(this).css('fill', '#'+ic); })
      .on("mouseout",  function(){ $(this).css('fill', '#'+nc); })
      ;
    $("text.preview") // UPDATE LABEL HOVER EFFECT
      .on("mouseover", function(){ $("#"+this.id.replace('text_', 'node_')).css('fill', '#'+ic); })
      .on("mouseout",  function(){ $("#"+this.id.replace('text_', 'node_')).css('fill', '#'+nc); })
      ;
  }
}

function SetBranches(){ // SET BRANCH SIZE AND COLOR
  var bc = "000000";
  var bs = 1.5;
  if ($('#branchcolor').length > 0 && $('#branchsize').length > 0) {
     bc = document.getElementById('branchcolor').value; // node color
     bs = parseFloat(document.getElementById('branchsize').value); // node color
  }
  if(bs && bc){
    $('path')
      .css('stroke-width',bs+'px')
      .css('stroke','#'+bc)
      ;
   }
}

function SetLabel(){ // LABEL INTERACTION
  d3.selectAll("text.preview")
    .call(drag)                                    // ADD dragable feature
    .on("click", function(){ move2front(this); })  // ADD label/image arrage feature
    ;
}

function SetImages(){
   v = document.getElementById('imagesize').value;
   if (v){
      d3.selectAll("svg image")
         .attr("width", v+"px");
      window.imageSize = parseInt(v);
   }
}


function ResetImages(){
   // DESELECTS ALL ALLELES AND REMOVES ALL IMAGES
   d3.select('#allelelist option').forEach(function(opt){ opt.selected=false; }); 
   d3.selectAll("svg image").remove(); 
}


function AddPredictions(path){
   // GET PREDITIONS from path through d3 and add them to allelelist
   d3.text(path, function(text) {
         if(text != null){
            var pred = {};
            f = text.split(/\r\n|\n/);
            var pred = {};
            for(var i=0; i<f.length; i++){
               var line = f[i]; // GET LINE
               if(line.charAt(0) === '#'){ continue; } // SKIP COMMENTS AND HEADER
               var e = line.split(/\s+/); // NAME  PREDICTION
               if(e.length == 2){
                  // Add Allele to pred
                  pred[e[0]] = e[1];
               }else if(line!=''){
                  console.log('Error in file format!\nlength='+e.length+'\n"'+line+'"');
               }
            }
            // UPDATE ALLELELIST WITH PREDICTIONS
            var options = d3.selectAll("#allelelist option")[0];
            for(var i=0; i<options.length; i++){
               var txt = options[i].innerHTML;
               if(txt in pred){ options[i].innerHTML += ' ('+ pred[txt] +')'; } // ADDING PREDICTION
            }
         }else{ // No File, Return Empty
            console.log('No Prediction file');
         }
      });
}


function MakeAllelist(){ // Populate the allele list with the labels found in the tree.
   // CLEAR ANY PREVIOUS LIST
   clearOptionList('allelelist');
   // CREATING LIST OF ALLELES
   var list = [];
   d3.selectAll("text.name").forEach(function(elem){
     for(var i=0; i<elem.length;i++){
      var txt = elem[i].textContent;                            // Option Text
      var val = txt.replace(/[^a-zA-Z0-9\_\-\.]/g,'');          // Option Value
      var id = 'option_'+txt.replace(':','_').replace(/[^a-zA-Z0-9\_\-]/g,''); // Option ID
      list.push([txt, val, id]);
     }
     // SORTING THE ALLELES ALPHABETICALLY
     list  = [].slice.call(list).sort(function(a,b){
           return a[1] < b[1] ? -1 : 1;
         });
     // APPENDING THE SORTED ALLELES AS OPTIONS TO THE allelelist SELECTOR
     for(var i=0; i<elem.length;i++){
      appendOptionList('allelelist', list[i][0], list[i][1], list[i][2]); //selector, text, value, id
     }
   });
   // Add preview class to these options and double click to remove image feature
   d3.selectAll("#allelelist option")
      .attr('class', 'preview')
      .on("dblclick", function(d,i){ RmImg('preview'+i); });
   // Add prediction scores to allelelist
   AddPredictions(window.imageDir+'/alleles.pred');
}

function clearOptionList(selector){
  /* This function removes elements from the selector */
	var elSel = document.getElementById(selector);
	while(elSel.length > 0){
		  elSel.remove(0); //elSel.length - 1
	}
}

function appendOptionList(selector, text, value, id){
  /* This function adds an option to the selector */
	var elOptNew = document.createElement('option');
	elOptNew.text = text;
	elOptNew.value = value;
   if(id){ elOptNew.id = id; } // Set optional id
	var elSel = document.getElementById(selector);
	try {
		elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
	} catch(ex) {
		elSel.add(elOptNew); // IE only
	}
}

function ResetUI(){
   // RESET ALL OPTIONS AND RELOAD TREE
   document.getElementById('dimx').value=800;
   document.getElementById('dimy').value=500;
   document.getElementById('marg').value=0;
   document.getElementById('span').value=360;
   document.getElementById('rotation').value=0;
   document.getElementById('lsize').value=8;
   document.getElementById('lcolor').value='000000';
   document.getElementById('lfont').selectedIndex=0;
   document.getElementById('bsize').value=6;
   document.getElementById('bcolor').value='000000';
   document.getElementById('bfont').selectedIndex=0;
   document.getElementById('branchsize').value='1.5';
   document.getElementById('branchcolor').value='C0C0C0';
   document.getElementById('nodesize').value='3';
   document.getElementById('nodecolor').value='0000BB';
   document.getElementById('imagesize').value='200';
   ResetImages();
   SetImages();
   ShowNewick();
}

function SelectLabels(){
   /* Select labels which contains the provided prefix in their names
    */
   // Remove previous selection
   d3.selectAll('text.name.selected').classed('selected', false);
   var str = document.getElementById('findlabels');
   d3.selectAll('text.name').each(function(d) {
         if(~this.id.indexOf(str.value.replace(':','_'))){ 
            d3.select(this).classed('selected', true);
            console.log( this );
         }
      });
}
function Colorselected(){
   /* Color the selected labels with the selected color.
    */
   var color = document.getElementById('findcolor').value;
   d3.selectAll('text.name.selected').style('fill','#'+color).classed('selected', false);
}

function LoadSVG(fileinput){
   /* This script will load a file containing an SVG image, and load it into
    * the svg place holder.
    * 
    * AUTHOR: Martin CF Thomsen
    * REQUIREMENTS: HTML 5
    * USAGE:
    *    <input type="file" id="files" onchange='LoadSVG(this)'/>
    *    <div id='svgholder'></div>
    */
   var fileObject = fileinput.files[0];
   // Validate File Type
    if( fileObject.type == "image/svg+xml"){
      // Initialize FileReader
      var reader = new FileReader();
      // On File Load
      reader.onload = function(e){  
         // Update SVG
         document.getElementsByTagName('svg')[0].outerHTML = reader.result;
         ActivateNodes();
      }
      // On Error
      reader.onerror = function() { console.log('Error reading file');}
      // Read File
      reader.readAsText(fileObject)
   }else{
      alert('The file you uploaded is not valid, please upload an SVG file! '+ fileinput.type);
   }
}