/***** OBJECTS *****
 *  Tree object == hierarchy of node objects
 *  
 * # Node object
 *   > parent node object pointer
 *   > children objects
 *   - x coordinate
 *   - y coordinate
 *   - r rotation
 *   > circle object
 *   > label object (none is not an end node)
 *   > branch object (none if it is the primary node, and the tree is unrooted)
 *
 * # Branch object
 *   - parent node object pointer
 *   - x0 coordinate (inherited from parent.parent.x)
 *   - y0 coordinate (inherited from parent.parent.y)
 *   - x1 coordinate (inherited from parent.x))
 *   - y1 coordinate (inherited from parent.y))
 *   - stroke width
 *   - text
 *   - color (global)
 *   - size  (global)
 *   - font  (global)
 *
 * # Circle object
 *   - parent node object pointer
 *   - x coordinate (inherited + offset)
 *   - y coordinate (inherited + offset)
 *   - circle size
 *   - filled (filled circle or hollow, yes/no)
 *   - circle color
 *
 * # Label object
 *   - parent node object pointer
 *   - x coordinate (inherited + offset)
 *   - y coordinate (inherited + offset)
 *   - rotation (inherited)
 *   - text
 *   - size  (global)
 *   - font  (global)
 *   - color (global)
 * 
 */

/***** METHODS *****
 * # Newick to json
 * # json to tree (Calculate tree layout/nodes)
 *   - find hierarchy
 *   - find node locations, rotation
 *
 * # Draw label
 * # Draw circle
 * # Draw branch
 */ 



//var diameter = 960;
//
//var tree = d3.layout.tree()
//    .size([360, diameter / 2 - 120])
//    .separation(function(a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });

//var diagonal = d3.svg.diagonal.radial()
//    .projection(function(d) { return [d.y, d.x / 180 * Math.PI]; });

var svg = d3.select("body").append("svg")
    .attr("width", diameter)
    .attr("height", diameter - 150)
  .append("g")
    .attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

d3.json("flare.json", function(error, root) {
  var nodes = tree.nodes(root),
      links = tree.links(nodes);

  var link = svg.selectAll(".link")
      .data(links)
    .enter().append("path")
      .attr("class", "link")
      .attr("d", diagonal);

  var node = svg.selectAll(".node")
      .data(nodes)
    .enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })

  node.append("circle")
      .attr("r", 4.5);

  node.append("text")
      .attr("dy", ".31em")
      .attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
      .attr("transform", function(d) { return d.x < 180 ? "translate(8)" : "rotate(180)translate(-8)"; })
      .text(function(d) { return d.name; });
});

d3.select(self.frameElement).style("height", diameter - 150 + "px");