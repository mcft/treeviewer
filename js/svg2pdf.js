/* NAME:    svg2pdf.js - SVG download scripts (TreeViewer)
 * AUTHOR:  Martin Thomsen
 * AFF:     Center for Biological Sequence Analysis, Technical University of Denmark
 * CONTACT: mcft AT cbs DOT dtu DOT dk
 * CITE:    M.Thomsen, O.Lund, C.Lundegaard and M. Nielsen, "MHCcluster, a
            method for functional clustering of MHC molecules." Immunogenetics,
            2013, ED: p. XXX-YYY. (Publication Underway)
 * DESC:    This library extends the tree builder script 'newick2svg.js' by
            adding a download feature to the Scalable Vector Graphics
            (SVG).
 * FEATS:   Download script
 * DEPENDS: jquery.js, svgconvert.cgi
            (github.com/agordon/d3export_demo/blob/master/download.pl),
            rsvg-convert (git.gnome.org/browse/librsvg)
 */

function svgconvert(format, data){
   /* This function calls a convertion script which convert a SVG to requested
    * format (SVG, PDF, or PNG) and return the converted image for download. 
    * Inputs are:
    *    SVG XML as text string
    *    Output format as string
    */
   //console.log(format, data);
   $form = $("<form method='post' action='http://www.cbs.dtu.dk/cgi-bin/svgconvert.cgi'></form>");
   $form.append("<input type  ='hidden' \
                        name  ='output_format' \
                        value ='"+format+"'>");
   $form.append("<input type  ='hidden' \
                        name  ='data' \
                        value ='"+data+"'>");
   //$form.append("<input type  ='submit'>");
   $('body').append($form);
   $form.submit();
   $form.remove();
}

function svgdownload(format){
   /* This function extracts a SVG and parse it to the svgconvert script.
    * Inputs are:
    *    Output format as string (SVG, PDF, or PNG)
    */
   // Get SVG
	var svg = document.getElementsByTagName("svg")[0];
   //console.log(svg);
	// Extract the data as SVG text string
	var data = (new XMLSerializer).serializeToString(svg);
   //console.log(data);
   // Call conversion script
   svgconvert(format, add_full_image_path(data));
}

function add_full_image_path(svg) {
   // Remove id attributes
   //console.log(svg, '\n');
   svg = svg.replace(/id="[\w\-]+"/g, '');
   //console.log(svg.match(/id="[\w\-]+"/g));
   // Fix missing xlink tags on images
   //console.log(svg.match(/image href/g));
   svg = svg.replace(/image href/g, 'image xlink:href');
   // Replace image path with full server path
   svg = svg.replace(/\/services\//g, "/usr/opt/www/pub/CBS/services/");
   //console.log(svg);
   return svg; 
}

//function getBase64Image(img) {
//    // Create an empty canvas element
//    var canvas = document.createElement("canvas");
//    canvas.width = img.width;
//    canvas.height = img.height;
//
//    // Copy the image contents to the canvas
//    var ctx = canvas.getContext("2d");
//    ctx.drawImage(img, 0, 0);
//
//    // Get the data-URL formatted image
//    // Firefox supports PNG and JPEG. You could check img.src to
//    // guess the original format, but be aware the using "image/jpg"
//    // will re-encode the image.
//    var dataURL = canvas.toDataURL("image/png");
//
//    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
//}
//
//function test(args) {
//   //Hello
//   d3.selectAll("svg image").forEach(function(elem){
//         elem.href.replace();
//      });
//}